#ifndef _funciones_h
#define _funciones_h

int menu(char entrada[], char salida[]);

int adentro(int p1, int p2, int lab1, int lab2);

int lectura_archivo(char  entrada[], int laberinto[][2], int *cant_obstaculos);

void llenar_laberinto(int laberinto_entrada[][2], int laberinto_salida[][15], int cant_obstaculos);

void escritura_archivo(int laberinto_salida[][15], int largo, int alto, char salida[]);

#endif // MAIN2_H_INCLUDED
